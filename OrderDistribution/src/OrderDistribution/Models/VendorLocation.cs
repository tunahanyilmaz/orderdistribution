﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderDistribution.Models
{
    public class VendorLocation
    {
        public double Latitude { get; set; } //X
        public double Longitude { get; set; } //Y
    }
}
