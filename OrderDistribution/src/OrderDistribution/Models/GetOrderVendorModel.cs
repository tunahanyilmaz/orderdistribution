﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderDistribution.Models
{
    public class GetOrderVendorModel
    {
        public IList<OrderVendorModel> OrderVendorList { get; set; }

        public double TotalCost { get; set; }
    }
}
