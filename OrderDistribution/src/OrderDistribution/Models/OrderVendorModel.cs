﻿namespace OrderDistribution.Models
{
    public class OrderVendorModel
    {
        public OrderModel Order { get; set; }

        public VendorModel Vendor { get; set; }
    }
}
