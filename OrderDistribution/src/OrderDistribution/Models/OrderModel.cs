﻿using OrderDistribution.Attributes;

namespace OrderDistribution.Models
{
    [SpreadSheet(Name = "Siparişler")]
    public class OrderModel
    {
        [ExcellColumn(Name = "Sipariş Numarası")]
        public int OrderNo { get; set; }

        [ExcellColumn(Name = "Latitude")]
        public double Latitude { get; set; }

        [ExcellColumn(Name = "Longitude")]
        public double Longitude { get; set; }
    }
}