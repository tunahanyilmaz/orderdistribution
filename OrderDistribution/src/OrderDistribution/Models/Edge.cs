﻿using System;

namespace OrderDistribution.Models
{
    public class Edge : IComparable
    {
        private Vertex v1, v2;
        private double cost;
        private VendorLocation stringPosition;

        public Vertex V1
        {
            get
            {
                return v1;
            }
        }

        public Vertex V2
        {
            get
            {
                return v2;
            }
        }

        public double Cost
        {
            get
            {
                return cost;
            }
        }

        public VendorLocation StringPosition
        {
            get
            {
                return stringPosition;
            }
        }

        public Edge(Vertex v1, Vertex v2, double cost)
        {
            this.v1 = v1;
            this.v2 = v2;
            this.cost = cost;
        }

        public int CompareTo(object obj)
        {
            Edge e = (Edge)obj;
            return this.cost.CompareTo(e.cost);
        }

    }
}
