﻿using OrderDistribution.Attributes;

namespace OrderDistribution.Models
{
    [SpreadSheet(Name = "Bayi Koordinatları")]
    public class VendorModel
    {
        [ExcellColumn(Name = "Bayi Adı")]
        public string VendorName { get; set; }

        [ExcellColumn(Name = "Latitude")]
        public double Latitude { get; set; }

        [ExcellColumn(Name = "Longitude")]
        public double Longitude { get; set; }
    }
}