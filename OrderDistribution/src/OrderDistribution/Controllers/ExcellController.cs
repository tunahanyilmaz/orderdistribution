﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using OrderDistribution.DbContext;
using OrderDistribution.Extensions;
using OrderDistribution.GeneticAlgorithm;
using OrderDistribution.Models;

namespace OrderDistribution.Controllers
{
    [Route("api/ExcellController/[action]")]
    [ApiController]
    public class ExcellController : ControllerBase
    {
        private readonly ExcellContext ExcellContext;

        public ExcellController()
        {
            ExcellContext = new ExcellContext();
        }

        [HttpGet]
        public GetOrderVendorModel GetOrders()
        {
            var model = new GetOrderVendorModel() { OrderVendorList = GeneticAlgo.GetOrderVendorList() };
            model.TotalCost = GeneticAlgo.CurrentSolution.ObjectiveFunctionValue;
            return model;
        }

        [HttpGet]
        public IEnumerable<VendorModel> GetVendors()
        {
            return ExcellContext.GetList<VendorModel>();
        }
    }
}   