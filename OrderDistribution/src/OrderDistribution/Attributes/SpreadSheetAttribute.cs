﻿using System;

namespace OrderDistribution.Attributes
{
    public class SpreadSheetAttribute : Attribute
    {
        public string Name { get; set; }
    }
}
