﻿using System;

namespace OrderDistribution.Attributes
{
    public class ExcellColumnAttribute : Attribute
    {
        public string Name { get; set; }
    }
}
