﻿using OrderDistribution.DbContext;
using OrderDistribution.Extensions;
using OrderDistribution.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderDistribution.Aggregates
{
    public class OrderAggregate
    {
        private readonly ExcellContext _excellContext;

        public OrderAggregate()
        {
            _excellContext = new ExcellContext();
        }


        public IEnumerable<AirDistance> CalculateDistances()
        {
            IList<AirDistance> airDistances = new List<AirDistance>();

            foreach (OrderModel order in _excellContext.GetList<OrderModel>())
            {
                //alternate with LINQ
                //var distancesAndVendor = vendors.Select(x => new { distance = Extensions.Extensions.CalculateDistance(new double[] { x.Latitude, order.Latitude }, new double[] { x.Longitude, order.Longitude }), vendor = x.VendorName }).OrderBy(x => x.distance);
                AirDistance airDistance = null;

                foreach (var vendor in _excellContext.GetList<VendorModel>())
                {
                    if (airDistance == null)
                    {
                        airDistance = new AirDistance
                        (
                            Extensions.Extensions.CalculateDistance(new double[] { vendor.Latitude, order.Latitude }, new double[] { vendor.Longitude, order.Longitude }),
                            order.OrderNo,
                            vendor.VendorName
                        );
                    }
                    else
                    {
                        double distance = Extensions.Extensions.CalculateDistance(new double[] { vendor.Latitude, order.Latitude }, new double[] { vendor.Longitude, order.Longitude });

                        if (airDistance.Distance > distance)
                        {
                            airDistance = new AirDistance
                            (
                                Extensions.Extensions.CalculateDistance(new double[] { vendor.Latitude, order.Latitude }, new double[] { vendor.Longitude, order.Longitude }),
                                order.OrderNo,
                                vendor.VendorName
                            );
                        }
                    }
                }

                //airDistances.Add(new AirDistance() { Distance = distancesAndVendor.First().distance, OrderNo = order.OrderNo, VendorName = distancesAndVendor.First().vendor });
                airDistances.Add(airDistance);
            }

            return airDistances.AsEnumerable();
        }
    }


    public class AirDistance
    {
        public int OrderNo { get; set; }

        public string VendorName { get; set; }

        public double Distance { get; set; }

        public AirDistance(double distance, int orderNo, string vendorName)
        {
            Distance = distance;
            OrderNo = orderNo;
            VendorName = vendorName;
        }

        public AirDistance()
        {
        }
    }
}