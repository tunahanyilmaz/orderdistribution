﻿//https://developers.google.com/maps/documentation/javascript/examples/marker-simple
//Drawing on the map
function initMap() {

    $.ajax({
        type: "GET",
        url: 'api/ExcellController/GetOrders',
        dataType: 'json',
        error: function (_jqXHR, _textStatus, thrownErr) {
            alert("Something went wrong! Reload the page.");
        },
        success: function (data) {
            map = new google.maps.Map(document.getElementById('map'), {
                center: { lat: 41.067156, lng: 29.017450 },
                zoom: 13
            });

            var shape = {
                coords: [1, 1, 1, 20, 18, 20, 18, 1],
                type: 'poly'
            };

            var renks = ["Kırmızı", "Mavi", "Yeşil"];

            for (var i = 0; i < data.orderVendorList.length; i++) {
                var image = {
                    url: 'images/order.png',

                    // This marker is 32 pixels wide by 32 pixels high.
                    size: new google.maps.Size(32, 32),
                    // The origin for this image is (0, 0).
                    origin: new google.maps.Point(0, 0),
                    // The anchor for this image is the base of the flagpole at (0, 32).
                    anchor: new google.maps.Point(16, 32)
                };

                var marker = new google.maps.Marker({
                    position: { lat: data.orderVendorList[i].order.latitude, lng: data.orderVendorList[i].order.longitude },
                    map: map,
                    icon: image
                    //shape: shape,
                    //title: data[0],
                    //zIndex: dataa[3]
                });
            }

            for (var i = 0; i < data.orderVendorList.length; i++) {
                var image = {
                    url: 'images/' + data.orderVendorList[i].vendor.vendorName + '-order.png',

                    // This marker is 32 pixels wide by 32 pixels high.
                    size: new google.maps.Size(32, 32),
                    // The origin for this image is (0, 0).
                    origin: new google.maps.Point(0, 0),
                    // The anchor for this image is the base of the flagpole at (0, 32).
                    anchor: new google.maps.Point(16, 32)
                };

                var marker = new google.maps.Marker({
                    position: { lat: data.orderVendorList[i].order.latitude, lng: data.orderVendorList[i].order.longitude },
                    map: map,
                    icon: image
                    //shape: shape,
                    //title: data[0],
                    //zIndex: dataa[3]
                });

                
            }
        }
    });
}


window.setInterval(function myfunction() {

    initMap();

}, 6000)