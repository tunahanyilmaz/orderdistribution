﻿namespace OrderDistribution.DbContext
{
    public class ExcellContext : Context
    {
        private const string PATH_SIPARIS_BAYI = "siparis ve bayi koordinatları.xlsx";

        public ExcellContext(string path = PATH_SIPARIS_BAYI) : base(path)
        {
        }
    }
}