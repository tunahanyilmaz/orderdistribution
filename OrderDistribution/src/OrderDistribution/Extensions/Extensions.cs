﻿using GeoCoordinatePortable;
using OrderDistribution.DbContext;
using OrderDistribution.GeneticAlgorithm;
using OrderDistribution.Helper;
using System.Collections.Generic;

namespace OrderDistribution.Extensions
{
    public static class Extensions
    {
        public static IEnumerable<T> GetList<T>(this Context context)
        {
            return ExcellHelper.GetDataFromExcellSheet<T>(context.PATH);
        }

        //public static IEnumerable<OrderModel> ToCoordinate(this IEnumerable<OrderModel> orderList)
        //{
        //    OrderAggregate aggregate = new OrderAggregate(orderList);
        //    return aggregate;
        //}

        /// <summary>
        /// Gets 2 latitude and 2 longitude. calculate air distance between them.
        /// </summary>
        /// <param name="latitude">array of latitude. used indexes : [0] and [1]</param>
        /// <param name="longitude">array of longitude. used indexes : [0] and [1]</param>
        /// <returns></returns>
        public static double CalculateDistance(double[] latitude, double[] longitude)
        {
            var sCoord = new GeoCoordinate(latitude[0], longitude[0]);
            var eCoord = new GeoCoordinate(latitude[1], longitude[0]);

            return sCoord.GetDistanceTo(eCoord);
        }

        public static bool IsFitToObjFnc(this int[] gen)
        {
            ObjectiveFunction objectiveFunction = new ObjectiveFunction();

            return objectiveFunction.IsSuitable(gen);
        }
    }
}