﻿using OrderDistribution.Enums;
using OrderDistribution.GeneticAlgorithm.Constants;
using OrderDistribution.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using OrderDistribution.Extensions;
using OrderDistribution.DbContext;

namespace OrderDistribution.GeneticAlgorithm
{
    public class ObjectiveFunction
    {
        static readonly ExcellContext context;

        static readonly IList<OrderModel> orderList;

        static ObjectiveFunction()
        {
            context = new ExcellContext();
            orderList = context.GetList<OrderModel>().ToList();
        }

        public bool IsSuitable(int[] individual)
        {
            int redCount = 0;
            int blueCount = 0;
            int greenCount = 0;

            foreach (var gen in individual)
            {
                if (gen == (int)VendorType.Blue)
                    blueCount++;
                else if (gen == (int)VendorType.Red)
                    redCount++;
                else if (gen == (int)VendorType.Green)
                    greenCount++;
            }

            if ((redCount < GeneticAlgorithmConstants.MaxRedVendor && redCount > GeneticAlgorithmConstants.MinRedVendor) &&
                (blueCount < GeneticAlgorithmConstants.MaxBlueVendor && blueCount > GeneticAlgorithmConstants.MinBlueVendor) &&
                (greenCount < GeneticAlgorithmConstants.MaxGreenVendor && greenCount > GeneticAlgorithmConstants.MinGreenVendor))
                return true;

            return false;
        }

        public CandidateSolution CreateRandomChild()
        {
            int[] newChild = new int[100];

            Random rnd = new Random();

            double redDivisionFactor = (GeneticAlgorithmConstants.MaxRedVendor + GeneticAlgorithmConstants.MinRedVendor) / 2;
            double blueDivisionFactor = (GeneticAlgorithmConstants.MaxBlueVendor + GeneticAlgorithmConstants.MinBlueVendor) / 2;
            double greenDivisionFactor = (GeneticAlgorithmConstants.MaxGreenVendor + GeneticAlgorithmConstants.MinGreenVendor) / 2;

            double redProbability = redDivisionFactor / (redDivisionFactor + blueDivisionFactor + greenDivisionFactor);
            double blueProbability = blueDivisionFactor / (redDivisionFactor + blueDivisionFactor + greenDivisionFactor);
            double greenProbability = greenDivisionFactor / (redDivisionFactor + blueDivisionFactor + greenDivisionFactor);

            for (int i = 0; i < newChild.Length; i++)
            {
                double temp = rnd.NextDouble();

                if (temp < redProbability)
                    newChild[i] = (int)VendorType.Red;
                else if (temp < greenProbability + redProbability)
                    newChild[i] = (int)VendorType.Green;
                else if (temp < redProbability + greenProbability + blueProbability)
                    newChild[i] = (int)VendorType.Blue;
            }


            return new CandidateSolution() { Individual = newChild };
        }

        public void CalculateObjFnc(CandidateSolution candidateSolution)
        {
            IList<VendorLineHolder> redVendorList = candidateSolution.Individual.Where(r => r.Equals((int)VendorType.Red)).Select((r, i) => new VendorLineHolder() { index = i }).ToList();
            IList<VendorLineHolder> greenVendorList = candidateSolution.Individual.Where(g => g.Equals((int)VendorType.Green)).Select((g, i) => new VendorLineHolder() { index = i }).ToList();
            IList<VendorLineHolder> blueVendorList = candidateSolution.Individual.Where(b => b.Equals((int)VendorType.Blue)).Select((b, i) => new VendorLineHolder() { index = i }).ToList();

            double redCost = CalculateCost(candidateSolution, redVendorList);
            double greenCost = CalculateCost(candidateSolution, greenVendorList);
            double blueCost = CalculateCost(candidateSolution, blueVendorList);

            candidateSolution.ObjectiveFunctionValue = redCost + greenCost + blueCost;
        }

        public double CalculateCost(CandidateSolution candidateSolution, IList<VendorLineHolder> vendorLine)
        {
            IList<Vertex> vertexes = new List<Vertex>();
            IList<Edge> edges = new List<Edge>();

            for (int i = 0; i < vendorLine.Count; i++)
            {
                for (int j = 0; j < vendorLine.Count; j++)
                {
                    VendorLineHolder holder1 = vendorLine[i];
                    VendorLineHolder holder2 = vendorLine[j];

                    if (holder1.index.Equals(holder2.index) == false)
                    {
                        if(vertexes.Any(v => v.Name.Equals(holder1.index)) == false)
                            vertexes.Add(new Vertex(holder1.index, new VendorLocation() { Latitude = orderList[holder1.index].Latitude, Longitude = orderList[holder1.index].Longitude }));

                        if (vertexes.Any(v => v.Name.Equals(holder2.index)) == false)
                            vertexes.Add(new Vertex(holder2.index, new VendorLocation() { Latitude = orderList[holder2.index].Latitude, Longitude = orderList[holder2.index].Longitude }));

                        Vertex v1 = vertexes.FirstOrDefault(v => v.Name.Equals(holder1.index));
                        Vertex v2 = vertexes.FirstOrDefault(v => v.Name.Equals(holder2.index));

                        Edge edge = new Edge(v1, v2, Extensions.Extensions.CalculateDistance(new double[2] { v1.Position.Latitude, v2.Position.Latitude }, new double[2] { v1.Position.Longitude, v2.Position.Longitude }));

                        if (edge.Cost != 0)
                            edges.Add(edge);
                    }
                }
            }
            Kruskal.Kruskal kruskal = new Kruskal.Kruskal();
            double cost = 0D;
            kruskal.Solve(edges, out cost);
            return cost;
        }
    }
}
