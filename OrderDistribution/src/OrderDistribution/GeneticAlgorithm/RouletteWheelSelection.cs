﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderDistribution.GeneticAlgorithm
{
    public class RouletteWheelSelection
    {
        public int[] gens = new int[100];

        public IList<CandidateSolution> Perform(IList<CandidateSolution> population)
        {
            double totalObjFnc = population.Sum(individual => individual.ObjectiveFunctionValue);

            double totalFactor = 0d;

            foreach (var individual in population)
            {
                totalFactor = totalFactor + individual.ObjectiveFunctionValue / totalObjFnc;
                individual.RoulettePobabilityValue = totalFactor;
            }

            IList<CandidateSolution> newPopulation = new List<CandidateSolution>();

            for (int i = 0; i < 100; i++)
            {
                double randomNumber = (new Random()).NextDouble();

                foreach (var individual in population.Reverse())
                {
                    if (randomNumber > individual.RoulettePobabilityValue)
                    {
                        newPopulation.Add(individual.Clone());
                        break;
                    }
                }

                if (newPopulation.Count < i + 1)
                    i--;
            }

            return newPopulation;
        }
    }
}
