﻿using OrderDistribution.Extensions;
using System;
using System.Collections.Generic;

namespace OrderDistribution.GeneticAlgorithm
{
    /// <summary>
    /// ordinary crossover
    /// </summary>
    public class CrossOver
    {
        public Random rnd { get; set; }

        public const int MAX_PROBAB = 5;

        public CrossOver()
        {
            rnd = new Random();
        }

        public IList<CandidateSolution> Perform(CandidateSolution mother, CandidateSolution father)
        {
            int crossOverPosition = mother.Individual.Length / 2;
            IList<CandidateSolution> newChilds = new List<CandidateSolution>();
            newChilds.Add(new CandidateSolution());
            newChilds.Add(new CandidateSolution());

            bool fitFunction = false;

            int j = 0;
            while (j < MAX_PROBAB)
            {

                for (int i = 0; i < mother.Individual.Length; i++)
                {
                    if (i < crossOverPosition)
                    {
                        newChilds[0].Individual[i] = mother.Individual[i];
                        newChilds[1].Individual[i] = father.Individual[i];

                    }
                    else
                    {
                        newChilds[0].Individual[i] = father.Individual[i];
                        newChilds[1].Individual[i] = mother.Individual[i];
                    }
                }

                if ((newChilds[0].Individual.IsFitToObjFnc() && newChilds[1].Individual.IsFitToObjFnc()) == false)
                {
                    newChilds[0].Individual = new int[mother.Individual.Length];
                    newChilds[1].Individual = new int[mother.Individual.Length];
                    j++;
                }
                else
                {
                    fitFunction = true;
                    break;
                }


                crossOverPosition = rnd.Next(1, mother.Individual.Length - 1);
            }
            if (fitFunction == false)
                newChilds = new List<CandidateSolution>()
                {
                    (new ObjectiveFunction()).CreateRandomChild(),
                    (new ObjectiveFunction()).CreateRandomChild()
                };

            return newChilds;
        }
    }
}
