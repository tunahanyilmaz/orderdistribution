﻿using System;
using System.Collections.Generic;

namespace OrderDistribution.GeneticAlgorithm
{
    public class Mutation
    {
        public double MutationVal { get; set; } = 0.05;

        public List<CandidateSolution> Perform(List<CandidateSolution> population)
        {
            Random random = new Random();

            foreach (var item in population)
            {
                if (random.NextDouble() < MutationVal)
                {
                    int firstGen = random.Next(0, 100);
                    int secondGen = random.Next(0, 100);

                    int temp = item.Individual[firstGen];
                    item.Individual[firstGen] = item.Individual[secondGen];
                    item.Individual[secondGen] = temp;
                }
            }

            return population;
        }
    }
}