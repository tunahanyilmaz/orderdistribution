﻿namespace OrderDistribution.GeneticAlgorithm
{
    public class CandidateSolution
    {
        public int[] Individual = new int[100];

        public double ObjectiveFunctionValue { get; set; }

        public double RoulettePobabilityValue { get; set; }

        public CandidateSolution Clone()
        {
            CandidateSolution candidateSolution = new CandidateSolution();
            candidateSolution.Individual = (int[])Individual.Clone();
            candidateSolution.ObjectiveFunctionValue = ObjectiveFunctionValue;
            candidateSolution.RoulettePobabilityValue = RoulettePobabilityValue;
            return candidateSolution;
        }
    }
}