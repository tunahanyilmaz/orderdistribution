﻿namespace OrderDistribution.GeneticAlgorithm.Constants
{
    public class GeneticAlgorithmConstants
    {
        public const int MaxRedVendor = 30;
        public const int MaxGreenVendor = 50;
        public const int MaxBlueVendor = 80;

        public const int MinRedVendor = 20;
        public const int MinGreenVendor = 35;
        public const int MinBlueVendor = 20;
    }
}
