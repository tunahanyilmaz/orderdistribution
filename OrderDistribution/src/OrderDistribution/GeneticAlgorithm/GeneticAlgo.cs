﻿using OrderDistribution.DbContext;
using OrderDistribution.Extensions;
using OrderDistribution.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderDistribution.GeneticAlgorithm
{
    public class GeneticAlgo
    {
        public static IEnumerable<VendorModel> VendorList { get; set; }

        public static IEnumerable<OrderModel> OrderList { get; set; }

        public static CandidateSolution CurrentSolution { get; set; }

        public static IList<CandidateSolution> CurrentPopulation { get; set; }

        public static ExcellContext ExcellContext { get; set; }

        public static IList<OrderVendorModel> OrderVendorList { get; set; } 

        static GeneticAlgo()
        {
            OrderVendorList = new List<OrderVendorModel>();

            ExcellContext = new ExcellContext();

            VendorList = ExcellContext.GetList<VendorModel>();

            OrderList = ExcellContext.GetList<OrderModel>();

            Initialize();

            Task.Factory.StartNew(() => StartGeneticAlgorithm());
        }

        private static Task StartGeneticAlgorithm()
        {
            while (true)
            {
                IList<CandidateSolution> candidateSolutions = PerformSelectionAndCreateNewPopulation(CurrentPopulation);

                candidateSolutions = PerformCrossOver(candidateSolutions);

                candidateSolutions = PerformMutation(candidateSolutions.ToList());

                CalculateObjFnc(candidateSolutions);

                CandidateSolution candidateSolution = candidateSolutions.OrderBy(s => s.ObjectiveFunctionValue).FirstOrDefault();

                int index = candidateSolutions.IndexOf(candidateSolution);

                candidateSolutions[index] = CurrentSolution.Clone();

                CurrentSolution = candidateSolutions.OrderBy(i => i.ObjectiveFunctionValue).FirstOrDefault();
            }
        }

        public static IList<CandidateSolution> CreatePopulation(int size)
        {
            IList<CandidateSolution> population = new List<CandidateSolution>();

            ObjectiveFunction objectiveFunction = new ObjectiveFunction();


            for (int i = 0; i < size; i++)
            {
                population.Add(objectiveFunction.CreateRandomChild());
            }

            return population;
        }

        public static void CalculateObjFnc(IList<CandidateSolution> population)
        {
            ObjectiveFunction objectiveFunction = new ObjectiveFunction();

            foreach (var individual in population)
            {
                objectiveFunction.CalculateObjFnc(individual);
            }
        }

        public static List<CandidateSolution> PerformCrossOver(IList<CandidateSolution> population)
        {
            List<CandidateSolution> candidateSolutions = new List<CandidateSolution>();

            CrossOver crossOver = new CrossOver();

            for (int i = 0; i < population.Count; i = i + 2)
            {
                candidateSolutions.AddRange(crossOver.Perform(population[i], population[i + 1]));
            }

            return candidateSolutions;
        }

        public static List<CandidateSolution> PerformMutation(List<CandidateSolution> population)
        {
            Mutation mutation = new Mutation();

            return mutation.Perform(population);
        }

        public static CandidateSolution GetElite(IList<CandidateSolution> candidateSolutions)
        {
            return candidateSolutions.OrderBy(x => x.ObjectiveFunctionValue).Last();
        }

        public static IList<CandidateSolution> PerformSelectionAndCreateNewPopulation(IList<CandidateSolution> candidateSolutions)
        {
            RouletteWheelSelection rouletteWheelSelection = new RouletteWheelSelection();
            return rouletteWheelSelection.Perform(candidateSolutions);
        }

        public static void Initialize()
        {
            var initialPopulation = CreatePopulation(100);

            CalculateObjFnc(initialPopulation);

            CurrentSolution = initialPopulation.OrderBy(i => i.ObjectiveFunctionValue).FirstOrDefault();

            CurrentPopulation = initialPopulation;
        }

        public static IList<OrderVendorModel> GetOrderVendorList()
        {
            return CurrentSolution.Individual.Select((inv, i) => new OrderVendorModel() { Order = OrderList.ToList()[i], Vendor = VendorList.ToList()[inv - 1] }).ToList();
        }
    }
}
